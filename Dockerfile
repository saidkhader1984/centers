FROM python:3.6.5
ENV PYTHONUNBUFFERED 1
RUN mkdir /azyan
WORKDIR /azyan
ADD requirements.txt /azyan/
RUN pip install -r requirements.txt
ADD . /azyan/