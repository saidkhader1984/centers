from rest_framework.test import APITestCase
from django.contrib.auth.models import User, Group
from django.conf import settings

import json

class UserTestCase(APITestCase):
    url = settings.CLIENT_URL + "users/"

    def setUp(self):
        self.admin_group = Group.objects.create(id=1, name="admin")
        self.user_group  = Group.objects.create(id=2, name="user")

        self.admin_user = User.objects.create(id=1,
                                    username="testuser",
                                    password="testuser",
                                    email="testuser@test.com",
                                    )
        self.admin_user.group = self.admin_group
        self.admin_user.save()

        self.user = User.objects.create(id=2,
                                    username="testuser1",
                                    password="testuser1",
                                    email="testuser1@test.com",
                                    )
        self.user.group = self.user_group
        self.user.save()

    def test_create_user_username_validation(self):
        """
        Test create user with username already exists
        """
        user_data = {
            "username": "testuser",
            "password": "test1234",
            "confirm_password": "test1234",
            "email": "testusertest@test.com",
        }

        response = self.client.post(self.url, user_data)
        self.assertEqual(400, response.status_code)

    def test_create_user_email_validation(self):
        """
        Test create user with email already exists
        """
        user_data = {
            "username": "testuserTest",
            "password": "test1234",
            "confirm_password": "test1234",
            "email": "testuser@test.com",
        }

        response = self.client.post(self.url, user_data)
        self.assertEqual(400, response.status_code)

    def test_create_user_password_validation(self):
        """
        Test to verify that a user call with invalid passwords
        """
        user_data = {
            "username": "testuserTest",
            "password": "test1234",
            "confirm_password": "1234",
            "email": "testuserTest@test.com",
        }

        response = self.client.post(self.url, user_data)
        self.assertEqual(400, response.status_code)

    def test_get_all_users_validation(self):
        """
        Test fetch all users
        """
        response = self.client.get(self.url)
        self.assertEqual(200, response.status_code)

    def test_get_user_validation(self):
        """
        Test fetch all user
        """
        url = self.url + "?q=" + self.admin_user.username
        response = self.client.get(url)
        self.assertEqual(200, response.status_code)

    def test_update_user_username_validation(self):
        """
        Test update username
        """
        url = self.url + str(self.user.id)  + "/"
        response = self.client.put(url, {"id": self.user.id, "username": "testing", "password": "testuser1"})
        response_data = json.loads(response.content)
        user = User.objects.get(id=self.user.id)
        self.assertEqual(response_data.get("username"), user.username)

    def test_update_user_username_exists_validation(self):
        """
        Test update username with username already exists
        """
        url = self.url + str(self.user.id)  + "/"
        response = self.client.put(url, {"username": "testuser"})
        response_data = json.loads(response.content)
        user = User.objects.get(id=self.user.id)
        self.assertEqual(response_data.get("username"), user.username)


    def test_delete_user_validation(self):
        """
        Test delete user
        """
        user = User.objects.create(username="test",
                                   password="test",
                                   email="test120@test.com",
                                   )
        user.group = self.admin_group
        user.save()

        url = self.url + str(user.id)  + "/"
        response = self.client.delete(url)
        self.assertEqual(204, response.status_code)


class GroupTestCase(APITestCase):
    url = "http://localhost:8000/groups/"

    def setUp(self):
        self.admin_group = Group.objects.create(id=1, name="admin")
        self.user_group  = Group.objects.create(id=2, name="user")

    def test_create_group_name_validation(self):
        """
        Test create group with name already exists
        """
        group_data = {
            "name": "admin",
        }

        response = self.client.post(self.url, group_data)
        self.assertEqual(400, response.status_code)

    def test_get_all_groups_validation(self):
        """
        Test fetch all groups
        """
        response = self.client.get(self.url)
        self.assertEqual(200, response.status_code)

    def test_get_group_validation(self):
        """
        Test fetch all centers
        """
        url = self.url + "?q=" + self.user_group.name
        response = self.client.get(url)
        self.assertEqual(200, response.status_code)

    def test_update_group_name_validation(self):
        """
        Test update group name 
        """
        url = self.url + str(self.admin_group.id)  + "/"
        response = self.client.put(url, {"name": "test"})
        response_data = json.loads(response.content)
        group = Group.objects.get(id=self.admin_group.id)
        self.assertEqual(response_data.get("name"), group.name)

    def test_update_group_name_exists_validation(self):
        """
        Test update group with name already exists
        """
        url = self.url + str(self.admin_group.id)  + "/"
        response = self.client.put(url, {"name": "admin"})
        response_data = json.loads(response.content)
        group = Group.objects.get(id=self.admin_group.id)
        self.assertEqual(response_data.get("name"), group.name)


    def test_delete_group_validation(self):
        """
        Test delete center
        """
        group = Group.objects.create(name="test")

        url = self.url + str(group.id)  + "/"
        response = self.client.delete(url)
        self.assertEqual(204, response.status_code)