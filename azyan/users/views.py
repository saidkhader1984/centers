from django.contrib.auth.models import User, Group
from django.contrib.auth.hashers import make_password
from rest_framework import viewsets
from django.http import HttpResponse
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework.exceptions import APIException
from rest_framework.response import Response
from rest_framework import status
from django.db.models import Q

import logging
import json

from azyan.users.serializers import UserSerializer, GroupSerializer
from azyan.utils.helpers import sendUserActivationEmail
from azyan.utils.permissions import Permission

class UserViewSet(viewsets.ModelViewSet):
    """
    This viewset automatically provides `list` and `detail` actions.
    """
    permission_classes = (AllowAny,)
    serializer_class = UserSerializer
    filter_fields = (
        "id",
        "username",
        "email",
    )

    def get_queryset(self, *args, **kwargs):
        users = User.objects.all()
        query = self.request.GET.get('q', None)

        if query:
            users = User.objects.filter(
                Q(username__icontains=query)|
                Q(email__icontains=query))

        return users
    
    def list(self, request):
        permission_classes = (IsAuthenticated,
                              Permission.isAdmin)

        return super(UserViewSet, self).list(request)            

    def retrieve(self, request, pk=None):
        return super(UserViewSet, self).retrieve(request, pk)

    def update(self, request, pk=None):
        permission_classes = (IsAuthenticated,
                              Permission.isAllowedToEditUser)
        
        return super(UserViewSet, self).update(request, pk)       

    def destroy(self, request, pk=None):
        permission_classes = (IsAuthenticated,
                              Permission.isAdmin)

        return super(UserViewSet, self).destroy(request, pk)

    def create(self, request, pk=None):
        permission_classes = (IsAuthenticated,
                              Permission.isAdmin)

        username  = request.data.get('username', None) 
        password  = request.data.get('password', None) 
        confirm_password = request.data.get('confirm_password', None) 
        email     = request.data.get('email', None) 
        group_id  = request.data.get('group_id', None) 

        if User.objects.filter(username=username).exists():
            msg = "USERNAME_ALREADY_EXISTS"
            return Response({"error": msg, "status_code": 400}, status=status.HTTP_400_BAD_REQUEST)
        
        if User.objects.filter(email=email).exists():
            msg = "EMAIL_ALREADY_EXISTS"
            return Response({"error": msg, "status_code": 400}, status=status.HTTP_400_BAD_REQUEST)

        if not Group.objects.filter(id=group_id).exists():
            msg = "GROUP_DOSE_NOT_EXISTS"
            return Response({"error": msg, "status_code": 400}, status=status.HTTP_400_BAD_REQUEST)

        if password != confirm_password:
            msg = "PASSWORD_DOSE_NOT_MATCH_CONFIRMATION"
            return Response({"error": msg, "status_code": 400}, status=status.HTTP_400_BAD_REQUEST)
        
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        # serializer.save()
        self.perform_create(serializer)

        user = User.objects.order_by('-pk')[0]
        user.password =  make_password(password)
        user.group_id = group_id
        user.save()

        serializered_user = UserSerializer(user).data
        headers = self.get_success_headers(serializered_user)
        return Response({"data": serializered_user}, status=status.HTTP_201_CREATED, headers=headers)

class GroupViewSet(viewsets.ModelViewSet):

    """
    This viewset automatically provides `list` and `detail` actions.
    """
    permission_classes = (AllowAny,)
    serializer_class = GroupSerializer
    queryset = Group.objects.all()
    filter_fields = (
        "id",
        "name",
    )
    
    def get_queryset(self, *args, **kwargs):
        permission_classes = (IsAuthenticated,
                              Permission.isAdmin)

        group = Group.objects.all()
        query = self.request.GET.get('q', None)

        if query:
            users = User.objects.filter(
                Q(name__icontains=query))

        return users
    

    def get_queryset(self):
        permission_classes = (IsAuthenticated,
                              Permission.isAdmin)

        return Group.objects.all()

    def list(self, request):
        permission_classes = (IsAuthenticated,
                              Permission.isAdmin)

        return super(GroupViewSet, self).list(request)

    def retrieve(self, request, pk=None):
        permission_classes = (IsAuthenticated,
                              Permission.isAdmin)

        return super(GroupViewSet, self).retrieve(request, pk)

    def update(self, request, pk=None):
        permission_classes = (IsAuthenticated,
                              Permission.isAdmin)

        return super(GroupViewSet, self).update(request, pk)

    def destroy(self, request, pk=None):
        permission_classes = (IsAuthenticated,
                              Permission.isAdmin)

        return super(GroupViewSet, self).destroy(request, pk)
    
    def create(self, request, pk=None):
        permission_classes = (IsAuthenticated,
                              Permission.isAdmin)
        
        name  = request.data.get('name', None) 
        if Group.objects.filter(name=name).exists():
            msg = "GROUP_ALREADY_EXISTS"
            return Response({"error": msg, "status_code": 400}, status=status.HTTP_400_BAD_REQUEST)
        
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)

        user = Group.objects.order_by('-pk')[0]
        
        serializered_user = GroupSerializer(user).data
        headers = self.get_success_headers(serializered_user)
        return Response({"data": serializered_user}, status=status.HTTP_201_CREATED, headers=headers)