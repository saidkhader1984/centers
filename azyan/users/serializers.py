from django.contrib.auth.models import User, Group
from rest_framework import serializers


class GroupSerializer(serializers.ModelSerializer):    
    class Meta:
        model  = Group
        fields = ('id', 'name')
        write_only_fields = ('name')
        read_only_fields = ('id', 'permissions')

class UserSerializer(serializers.ModelSerializer):
    groups = GroupSerializer(read_only=True)
    class Meta:
        model = User
        fields = ('id', 'username', 'email', 'password', 'groups')
        write_only_fields = ('password')
        read_only_fields = ('id','is_staff', 'is_superuser', 'is_active', 'date_joined')