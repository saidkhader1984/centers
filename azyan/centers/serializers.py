from rest_framework import serializers
from django.contrib.auth.models import User

from azyan.centers.models import Center
from azyan.users.serializers import UserSerializer


class CenterSerializer(serializers.HyperlinkedModelSerializer):
	user = UserSerializer(read_only=True)
	
	class Meta:
		model = Center
		fields = ('name', 'user', 'latitude', 'longitude')