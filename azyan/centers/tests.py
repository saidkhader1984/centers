from rest_framework.test import APITestCase
from django.contrib.auth.models import User, Group
from rest_framework_jwt.settings import api_settings
from django.conf import settings
from django.urls import reverse
from rest_framework.test import APIClient

from azyan.centers.models import Center
from azyan.centers.serializers import CenterSerializer

import json

jwt_payload_handler = api_settings.JWT_PAYLOAD_HANDLER
jwt_encode_handler = api_settings.JWT_ENCODE_HANDLER

class UserTestCase(APITestCase):
    url = settings.CLIENT_URL + "centers/"

    def setUp(self):
        self.admin_group = Group.objects.create(id=1, name="admin")
        self.user = User.objects.create(username="test1",
                                        email="test1@test.com",
                                        )
        self.user.set_password("test1111")
        self.user.group = self.admin_group
        self.user.is_active = True
        self.user.save()

        data = [{"name":"Ghazaal PC", "long":"31.9847221", "lat":"35.8837308"},
        		{"name":"Da3san Arcades", "long":"31.9853975", "lat":"35.8811971"},
        		{"name":"Shmeisani Arcades", "long":"31.9676002", "lat":"35.8984977"},
        		{"name":"Chili Dahyeh", "long":"31.9657544", "lat":"35.8834699"},
        		{"name":"Toys Wadi Saqra", "long":"31.9587564", "lat":"35.9073862"},
        		{"name":"Al-no3mani", "long":"31.9763312", "lat":"35.8560434"},
        		{"name":"Al-Doulab", "long":"31.9576477", "lat":"35.8671192"},
        		{"name":"Habeeba Balad", "long":"31.9519273", "lat":"35.9333741"},
        		{"name":"McDonalds", "long":"31.9742878", "lat":"35.8541098"},
        		{"name":"VBC Khalda", "long":"31.979656", "lat":"35.849959"}
               ]

        i = 1
        for center in data:
        	self.center = Center.objects.create(id=i,
	                                    name=center["name"],
	                                    user=self.user,
	                                    longitude=center["long"],
	                                    latitude=center["lat"]
	                                    )

        	i+=1


        self.api_authentication()

    def api_authentication(self):

        """
        Ensure JWT login view using form POST works.
        """
        url = settings.CLIENT_URL + 'api-auth/'

        data = {
            'username': self.user.username,
            'password': "test1111" 
        }

        response = self.client.post(url, data, format='json')
        response_data = json.loads(response.content)
        self.assertEqual(200, response.status_code)
        self.assertTrue('token' in response.data)
        self.token = response.data['token']

        self.client = APIClient()
        self.client.credentials(HTTP_AUTHORIZATION='JWT ' + self.token)
       
    def test_create_center_name_validation(self):
        """
        Test create center with name already exists
        """
        center_data = {
            "name": "McDonalds",
            "user_id": 1,
           	"long":"31.979656", 
           	"lat":"35.849959"
        }

        response = self.client.post(self.url, center_data)
        self.assertEqual(400, response.status_code)

    def test_get_all_centers_validation(self):
        """
        Test fetch all centers
        """
        response = self.client.get(self.url)
        self.assertEqual(200, response.status_code)

    def test_get_center_validation(self):
        """
        Test fetch all centers
        """
        url = self.url + "?q=" + self.center.name
        response = self.client.get(url)
        self.assertEqual(200, response.status_code)

    def test_update_center_name_validation(self):
        """
        Test update name
        """
        url = self.url + str(self.center.id)  + "/"
        response = self.client.put(url, {"name": "test"})
        # response_data = json.loads(response.content)
        # self.assertEqual(200, response_data)

        center = Center.objects.get(id=self.center.id)
        self.assertEqual(200, response.status_code)

    def test_delete_center_validation(self):
        """
        Test delete center
        """
        center = Center.objects.create(name="testForDelete",
                                        user=self.user,
                                        longitude="31.9847221",
                                        latitude="35.8837308"
                                        )

        url = self.url + str(center.id)  + "/"
        response = self.client.delete(url)
        # response_data = json.loads(response.content)
        # self.assertEqual(200, response_data)
        self.assertEqual(204, response.status_code)

    def test_find_distance_validation(self):
        """
        Test find istance
        """
        lang = "en"
        origin_long = "31.9847221"
        origin_lat  = "35.8837308"
        dest_long   = "31.9853975"
        dest_lat    = "35.8811971"

        center_data = {
            "origin_long": origin_long,
            "origin_lat": origin_lat,
            "dest_long": dest_long, 
            "dest_lat": dest_lat
        }

        url = self.url + "find_distance/"
        response = self.client.get(url, center_data)
        # response_data = json.loads(response.content)
        # self.assertEqual(201, response_data)
        self.assertEqual(201, response.status_code)

    def test_find_route_validation(self):
        """
        Test find route
        """
        lang = "en"
        origin_long = "31.9847221"
        origin_lat  = "35.8837308"
        dest_long   = "31.9853975"
        dest_lat    = "35.8811971"

        center_data = {
            "origin_long": origin_long,
            "origin_lat": origin_lat,
            "dest_long": dest_long, 
            "dest_lat": dest_lat
        }

        url = self.url + "find_route/"
        response = self.client.get(url, center_data)
        # response_data = json.loads(response.content)
        # self.assertEqual(201, response_data)
        self.assertEqual(201, response.status_code)