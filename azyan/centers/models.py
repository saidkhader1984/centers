from django.db import models
from django.contrib.auth.models import User

class Center(models.Model):
    name       = models.CharField(max_length=200, null=True, blank=True)
    user       = models.ForeignKey(User)
    latitude   = models.FloatField(blank=True, null=True, verbose_name='Latitude')
    longitude  = models.FloatField(blank=True, null=True, verbose_name='Longitude')
    created_on = models.DateField(auto_now_add=True)

    def __str__(self):
        return self.name