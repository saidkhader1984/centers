from rest_framework import viewsets
from django.http import HttpResponse
from django.contrib.auth.models import User
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework.exceptions import APIException
from rest_framework.response import Response
from rest_framework.decorators import list_route
from rest_framework import status
from django.conf import settings
from django.db.models import Q
import urllib.request, json

import logging
import json

from azyan.centers.serializers import CenterSerializer
from azyan.utils.permissions import Permission
from azyan.centers.models import Center

class CenterViewSet(viewsets.ModelViewSet):
    """
    This viewset automatically provides `list` and `detail` actions.
    """
    permission_classes = (IsAuthenticated,)
    serializer_class = CenterSerializer
    filter_fields = (
        "id",
        "latitude",
        "longitude",
        "name"
    )
    
    def get_queryset(self, *args, **kwargs):
        center = Center.objects.all()
        query  = self.request.GET.get('q', None)
        longitude = self.request.GET.get('long', None)
        latitude  = self.request.GET.get('lat', None)

        if query:
            center = Center.objects.filter(
                Q(name__icontains=query))

        if longitude and latitude:
            center = Center.objects.filter(
                Q(longitude=longitude) &
                Q(latitude=latitude))

        return center

    def list(self, request):
        return super(CenterViewSet, self).list(request)

    def retrieve(self, request, pk=None):
        return super(CenterViewSet, self).retrieve(request, pk)

    def update(self, request, pk=None):
        permission_classes = (IsAuthenticated,
                              Permission.isAllowedToEditCenter)

        return super(CenterViewSet, self).update(request, pk)  

    def destroy(self, request, pk=None):
        permission_classes = (IsAuthenticated,
                              Permission.isAllowedToEditCenter)

        return super(CenterViewSet, self).destroy(request, pk)
            
    def create(self, request, pk=None):
        name      = request.data.get('name')
        user_id   = request.data.get('user_id')
        latitude  = request.data.get('latitude')
        longitude = request.data.get('longitude')

        if Center.objects.filter(name=name).exists():
            msg = "CENTER_ALREADY_EXISTS"
            return Response({"error": msg, "status_code": 400}, status=status.HTTP_400_BAD_REQUEST)
        
        if not User.objects.filter(id=user_id).exists():
            msg = "USER_DOES_NOT_EXISTS"
            return Response({"error": msg, "status_code": 400}, status=status.HTTP_400_BAD_REQUEST)
       
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)

        center = Center.objects.order_by('-pk')[0]
        center.user_id =  user_id
        center.save()

        serializered_center = CenterSerializer(center).data
        headers = self.get_success_headers(serializered_center)
        return Response({"data": serializered_center}, status=status.HTTP_201_CREATED, headers=headers)

    @list_route(methods=["GET", ])
    def find_distance(self, request):
        origin_long = self.request.GET.get('origin_long', None)
        origin_lat  = self.request.GET.get('origin_lat', None)
        dest_long   = self.request.GET.get('dest_long', None)
        dest_lat    = self.request.GET.get('dest_lat', None)
        lang        = self.request.GET.get('lang', "en")


        if origin_long and origin_lat and dest_long and dest_lat:
            url = settings.GOOGLE_MAPS_ROUTE_URL
            origin      = str(origin_long) + "," + str(origin_lat)
            destination = str(dest_long) + "," + str(dest_lat)

            #Building the URL for the request
            nav_request = 'origin={}&destination={}&language={}&key={}'.format(origin, destination, lang, settings.GOOGLE_KEY)
            request = url + nav_request
            #Sends the request and reads the response.
            response = urllib.request.urlopen(request).read()
            #Loads response as JSON
            directions = json.loads(response)
            headers = self.get_success_headers(directions)
            return Response({"data": directions}, status=status.HTTP_201_CREATED, headers=headers)
        else:
            msg = "INVALID_DATA_FORMAT"
            return Response({"error": msg, "status_code": 400}, status=status.HTTP_400_BAD_REQUEST)

    @list_route(methods=["GET", ])
    def find_route(self, request):
        origin_long = self.request.GET.get('origin_long', None)
        origin_lat  = self.request.GET.get('origin_lat', None)
        dest_long   = self.request.GET.get('dest_long', None)
        dest_lat    = self.request.GET.get('dest_lat', None)
        lang        = self.request.GET.get('lang', "en")

        if origin_long and origin_lat and dest_long and dest_lat:
            url = settings.GOOGLE_MAPS_ROUTE_URL
            origin      = str(origin_long) + "," + str(origin_lat)
            destination = str(dest_long) + "," + str(dest_lat)
            
            #Building the URL for the request
            nav_request = 'origin={}&destination={}&language={}&key={}'.format(origin,destination,lang,settings.GOOGLE_KEY)
            request = url + nav_request
            #Sends the request and reads the response.
            response = urllib.request.urlopen(request).read()
            #Loads response as JSON
            directions = json.loads(response)
            headers = self.get_success_headers(directions)
            return Response({"data": directions}, status=status.HTTP_201_CREATED, headers=headers)
        else:
            msg = "INVALID_DATA_FORMAT"
            return Response({"error": msg, "status_code": 400}, status=status.HTTP_400_BAD_REQUEST)