import hashlib

def sendUserActivationEmail(user):
	user_name   = user.username
	hash_object = hashlib.md5(user_name.encode())
	hash_key    = hash_object.hexdigest()

	send_email('emails/invite_assistant.html', settings.DEFAULT_FROM_EMAIL, [email],
	           'Technical Assistance Invitation', {
	               'NAME': user.username,
	               'EMAIl': user.email,
	               'PASSWORD': user.password,
	               'key': hash_key,
	               'CLIENT_URL': settings.CLIENT_URL
	})
