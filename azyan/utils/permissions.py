from django.db.models import Q
from django.contrib.auth.models import User, Group
from rest_framework import permissions

from azyan.centers.models import Center

class Permission(permissions.BasePermission):
	def isAdmin(self, request):
		return self.request.user.group == "admin"

	def isAllowedToEditCenter(self, request):
		isOwner = Center.objects.get(id=id, user_id=request.user.id).exists()

		if self.is_admin(request) or isOwner:
			return True
		else:
			return False

	def isAllowedToEditUser(self, request):
		isOwner = User.objects.get(id=request.user.id).exists()

		if self.is_admin(request) or isOwner:
			return True
		else:
			return False