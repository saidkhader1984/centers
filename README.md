Install:
	- sudo docker-compose build
	- sudo docker-compose up

DataBase Migration:
	- sudo docker-compose exec web bash
	- python manage.py migrate
	- python manage.py createsuperuser

User API Endpoints:
 - Users:
 	http://localhost:8000/users/
 - Groups:
 	http://localhost:8000/groups/
 - Centers:
 	http://localhost:8000/centers/

Test Cases:
 - run the following commands:
 	sudo docker-compose exec web bash
 	./manage.py test azyan/users (for users and groups)
 	./manage.py test azyan/centers
